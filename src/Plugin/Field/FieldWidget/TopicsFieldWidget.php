<?php

namespace Drupal\hfc_topics_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'hfc_topics_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "hfc_topics_field_widget",
 *   label = @Translation("Core Course Topics"),
 *   field_types = {
 *     "hfc_topics_field_type"
 *   }
 * )
 */
class TopicsFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $element['topic'] = [
      '#type' => 'textfield',
      '#title' => t('Topic'),
      '#default_value' => isset($items[$delta]->topic) ? $items[$delta]->topic : NULL,
      '#size' => 100,
      '#maxlength' => 255,
    ];

    // @todo: needs to be text_format but blows up horribly when you do.
    $element['objective'] = [
      '#type' => 'textarea',
      '#title' => t('Objective'),
      '#default_value' => isset($items[$delta]->objective) ? $items[$delta]->objective : NULL,
      // '#format' => $items[$delta]->format,
      // '#base_type' => 'textarea',
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
    ];

    // I hate my life...
    $element['format'] = [
      '#type' => 'value',
      '#value' => 'markdown',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    if ($violation->arrayPropertyPath == ['format'] && isset($element['format']['#access']) && !$element['format']['#access']) {
      // Ignore validation errors for formats if formats may not be changed,
      // i.e. when existing formats become invalid. See filter_process_format().
      return FALSE;
    }
    return $element;
  }

}

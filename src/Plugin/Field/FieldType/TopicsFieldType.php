<?php

namespace Drupal\hfc_topics_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'hfc_topics_field_type' field type.
 *
 * @FieldType(
 *   id = "hfc_topics_field_type",
 *   label = @Translation("Core Course Topic"),
 *   description = @Translation("Provides a field type for HFC Core Course Topics and Objectives."),
 *   default_widget = "hfc_topics_field_widget",
 *   default_formatter = "hfc_topics_field_formatter",
 * )
 */
class TopicsFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['topic'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Topic value'))
      ->setRequired(TRUE);

    $properties['objective'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Objective value'));

    $properties['format'] = DataDefinition::create('filter_format')
      ->setLabel(t('Text format'));

    $properties['processed'] = DataDefinition::create('string')
      ->setLabel(t('Processed text'))
      ->setDescription(t('The text with the text format applied.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\text\TextProcessed')
      ->setSetting('text source', 'objective');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'topic' => [
          'type' => 'varchar',
          'length' => 2048,
        ],
        'objective' => [
          'type' => 'text',
          'size' => 'big',
        ],
        'format' => [
          'type' => 'varchar_ascii',
          'length' => 255,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE) {
    // @todo: (From core) Add in the filter default format here (properly).
    $this->setValue(['format' => 'markdown'], $notify);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $topic = $this->get('topic')->getValue();
    $objective = $this->get('objective')->getValue();
    return empty($topic) && empty($objective);
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    // Unset processed properties that are affected by the change.
    foreach ($this->definition->getPropertyDefinitions() as $property => $definition) {
      if ($definition->getClass() == '\Drupal\text\TextProcessed') {
        if ($property_name == 'format' || ($definition->getSetting('text source') == $property_name)) {
          $this->writePropertyValue($property, NULL);
        }
      }
    }
    parent::onChange($property_name, $notify);
  }

}

<?php

namespace Drupal\hfc_topics_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'hfc_topics_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "hfc_topics_field_formatter",
 *   label = @Translation("Core Course Topics"),
 *   field_types = {
 *     "hfc_topics_field_type"
 *   }
 * )
 */
class TopicsFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta]['topic'] = [
        '#plain_text' => $item->topic,
      ];
      $elements[$delta]['objective'] = [
        '#type' => 'processed_text',
        '#text' => $item->objective,
        '#format' => $item->format,
      ];
    }

    return $elements;
  }

}

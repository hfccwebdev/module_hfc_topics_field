<?php

namespace Drupal\hfc_topics_field\Plugin\diff\Field;

use Drupal\diff\FieldDiffBuilderBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin to compare the title and the uris of two link fields.
 *
 * @FieldDiffBuilder(
 *   id = "hfc_topics_field_diff_builder",
 *   label = @Translation("HFC Topics Field Diff"),
 *   field_types = {
 *     "hfc_topics_field_type"
 *   },
 * )
 */
class TopicsFieldBuilder extends FieldDiffBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function build(FieldItemListInterface $field_items) {

    $result = [];
    // Every item from $field_items is of type FieldItemInterface.
    foreach ($field_items as $field_key => $field_item) {
      $values = $field_item->getValue();

      // Compare field values.
      if (isset($values['objective'])) {
        $result[$field_key][] = $values['topic'] . ":\n" . $values['objective'];
      }
      else {
        $result[$field_key][] = $values['topic'];
      }
    }
    return $result;
  }

}
